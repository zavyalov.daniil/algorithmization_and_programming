using System;
namespace Tanks
{
    public interface IMoveExceptionHandler
    {
        ICommand cmd {get;}
        Type type {get;}
    }
}