﻿using System;
using System.IO;

namespace Tanks
{
    public 
    class Logger : ICommand
    {
        string message;
        public Logger(string message)
        {
            this.message = message;
        }

        public void Execute()
        {
            File.AppendAllText("log.txt", message);
        }
    }
}
