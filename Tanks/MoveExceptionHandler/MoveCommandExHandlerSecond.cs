using System;
namespace Tanks
{
   public class MoveCommandExHandlerSecond : ICommand
   {
        IMoveExceptionHandler interf;
        public MoveCommandExHandlerSecond (IMoveExceptionHandler interf)
        {
            this.interf = interf;
        }

        public void Execute()
        {
            try
            {
                interf.cmd.Execute();
            }
            catch(Exception ex)
            {
                if (interf.type == ex.GetType())
                {
                    IoC.Resolve<ICommand>("Log.Message", ex).Execute();
                }
                else
                {
                    throw ex;
                }
            }
        
        }
   }
}