using System;
namespace Tanks
{
    public class MoveCommandExHandlerFirst : ICommand
    {
        IMoveExceptionHandler interf;
        public MoveCommandExHandlerFirst (IMoveExceptionHandler interf)
        {
            this.interf = interf;
        }

        public void Execute()
        {
            try
            {
                interf.cmd.Execute();
            }
            catch(Exception ex)
            {
                if (interf.type == ex.GetType())
                {
                    IoC.Resolve<ICommand>("GiveCommand2", interf).Execute();
                }
                else
                {
                    throw ex; 
                }
            }
        
        }
    }
}