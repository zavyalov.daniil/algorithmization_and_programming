using System.Collections.Concurrent;

namespace Tanks
{
    public class OrderHandlerCmd:ICommand 
    {
        private ConcurrentQueue<IUObject> orderQueue;
        public OrderHandlerCmd(ConcurrentQueue<IUObject> _orderQueue) {
            orderQueue = _orderQueue;
        }
        public void Execute() {
            IUObject order;
            if (orderQueue.TryDequeue(out order))
            {
                IoC.Resolve<ICommand>("OrderInterpretCmd", order).Execute();
            }
        }
    }
}
