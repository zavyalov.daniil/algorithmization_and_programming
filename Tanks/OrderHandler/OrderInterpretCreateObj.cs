
namespace Tanks
{
    public class TacticsOrderInterpretCreateObj
    {
        public TacticsOrderInterpretCreateObj() {
            
        }
        public IOrderInterpretable Execute(params object[] args) {
            IUObject order = (IUObject)args[0];
            return new OrderInterpretable((string)order["Type"], order);
        }
    }
}
