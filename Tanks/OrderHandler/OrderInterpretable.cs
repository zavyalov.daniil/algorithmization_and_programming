namespace Tanks
{
    public class OrderInterpretable: IOrderInterpretable
    {
        private string orderType;
        private IUObject order;
        public OrderInterpretable(string _orderType, IUObject _order)
        {
            orderType = _orderType;
            order = _order;
        }
        public string OrderType
        {
            get
            {
                return orderType;
            }
        }
        public IUObject Order {
            get
            {
                return order;
            }
        }
    }
}
