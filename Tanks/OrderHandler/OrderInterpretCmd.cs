using System.Collections.Concurrent;

namespace Tanks
{
    public class OrderInterpretCmd:ICommand 
    {
        private IOrderInterpretable obj;
        public OrderInterpretCmd(IOrderInterpretable orderInterpret) {
            obj = orderInterpret;
        }
        public void Execute() {
            BlockingCollection<ICommand> commandQueue = IoC.Resolve<BlockingCollection<ICommand>>("Thread.CommandQueue");
            commandQueue.Add(IoC.Resolve<ICommand>("Orders.GetCommand", obj.OrderType, obj.Order));
        }
    }
}
