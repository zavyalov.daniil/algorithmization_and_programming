
namespace Tanks
{
    public class TacticsOrderInterpretCreate
    {
        public TacticsOrderInterpretCreate() {
            
        }
        public ICommand Execute(params object[] args) {
            return new OrderInterpretCmd(IoC.Resolve<IOrderInterpretable>("Adapter.IOrderInterpretable.Create", args[0]));
        }
    }
}
