namespace Tanks
{
    public interface IOrderInterpretable
    {
        public string OrderType
        {
            get;
        }
        public IUObject Order {
            get;
        }
    }
}
