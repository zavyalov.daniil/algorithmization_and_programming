using System.Collections.Concurrent;
using System;

namespace Tanks
{
	public interface IThreadProc
	{
		public static Action<BlockingCollection<ICommand>> ProcAct { get; set; }
		public static void ThreadProc(){}
	}
}
