using System;
using System.Collections.Generic;

namespace Tanks
{
	public interface ICollisionAnalysable
	{
		public List<int[,]> GetFirstSprite();
		public List<int[,]> GetSecondSprite();
	}
}
