using System;
using System.Collections.Generic;

namespace Tanks
{
	public interface IGetCollisison
	{
		public IntPair GetCenterCoords();
		public double GetFirstDirection();
		public double GetSecondDirection();
		public int[,] GetFirstArray();
		public int[,] GetSecondArray();
		public List<string> GetResList();
	}
}
