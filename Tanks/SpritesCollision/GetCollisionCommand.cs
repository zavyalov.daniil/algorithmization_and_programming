using System;
using System.Collections.Generic;

namespace Tanks
{
	public class GetCollisionCommand : ICommand
	{
		IntPair nPos;
		IntPair mPos; 
		double directionN;
		double directionM;
		int[,] n;
		int[,] m;
		List<string> res;
		int maxSpeed;

		public GetCollisionCommand(IGetCollisison source)
		{
			nPos = source.GetCenterCoords();
			mPos = new IntPair(0, 0);
			directionN = source.GetFirstDirection();
			directionM = source.GetSecondDirection();
			n = source.GetFirstArray();
			m = source.GetSecondArray();
			res = source.GetResList();
			maxSpeed = IoC.Resolve<int>("GetMaxSpeed");
		}
		public void Execute()
		{
			(bool, List<(int nXVelo,int nYVelo,int mXVelo,int mYVelo)>) preAnalysis = HaveTouch();
			if (!preAnalysis.Item1) { return; }
			else
			{
				var nMoved = n;
				foreach (var speed in preAnalysis.Item2)
				{
					IntPair nChangedCoord = new IntPair(nPos.X + speed.nXVelo, nPos.Y + speed.nYVelo);
					IntPair mChangedCoord = new IntPair(mPos.X + speed.mXVelo, mPos.Y + speed.mYVelo);
					IntPair deltaCoord = new IntPair(mChangedCoord.X-nChangedCoord.X,mChangedCoord.Y-nChangedCoord.Y);
					nMoved = MoveMatrix(nMoved, deltaCoord);
					if (ContainsTwo(MatrixSumm(nMoved,m))) 
					{
						string str = ""+deltaCoord.X + " "+ deltaCoord.Y + " " + (speed.mXVelo-speed.mXVelo) + " " + (speed.mYVelo-speed.mYVelo);
						res.Add(str);
					}
				}
			}

		}


		private (bool, List<(int nXVelo,int nYVelo,int mXVelo,int mYVelo)>) HaveTouch()
		{
			bool isContact = false;
			List<(int,int,int,int)> contactSpeed = new List<(int,int,int,int)>();
			for (int i = maxSpeed; i > 0; i--)
			{
				for (int j = maxSpeed; j > 0; j--)
				{
					(int xSpeed, int ySpeed) nVelocityVec = CalcVec(i, directionN);
					(int xSpeed, int ySpeed) mVelocityVec = CalcVec(j, directionM);
					IntPair nChangedCoord = new IntPair(nPos.X + nVelocityVec.xSpeed, nPos.Y + nVelocityVec.ySpeed);
					IntPair mChangedCoord = new IntPair(mPos.X + mVelocityVec.xSpeed, mPos.Y + mVelocityVec.ySpeed);
					IntPair distance = new IntPair(Math.Abs(nChangedCoord.X - mChangedCoord.X), Math.Abs(nChangedCoord.Y - mChangedCoord.Y));
					IntPair contactDistance = new IntPair((int)((n.GetLength(1) + m.GetLength(1)) / 2), (int)((n.GetLength(0) + m.GetLength(0)) / 2));

					if ((distance.X <= contactDistance.X) && (distance.Y <= contactDistance.Y))
					{
						isContact = true;
						contactSpeed.Add((nVelocityVec.xSpeed,nVelocityVec.ySpeed,mVelocityVec.xSpeed,mVelocityVec.ySpeed));
					}
				}

			}
			return (isContact, contactSpeed);
		}


		private int[,] MoveMatrix(int[,] n, IntPair deltaPos)
		{
			int[,] res = new int[n.GetLength(0),n.GetLength(1)];
			for (int i = 0; i<n.GetLength(0) ; i++) 
			{
				for (int j = 0; j<n.GetLength(1) ; j++)
				{
					if
					(
						((i+deltaPos.Y>0)&&(i+deltaPos.Y<n.GetLength(0)))
						&&
						((j+deltaPos.X>0)&&(j+deltaPos.X<n.GetLength(1)))
					)
					{
						res[i + deltaPos.Y, j + deltaPos.X] = n[i, j];
					}
					n[i, j] = 0;
				}
			}
			return res;
		}

		private int[,] MatrixSumm(int[,] n, int[,] m)
		{
			int[,] res = new int[n.GetLength(0),n.GetLength(1)];
			for (int i = 0; i < n.GetLength(0);i++)
			{
				for (int j = 0; j < n.GetLength(1);j++) 
				{
					res[i, j] = n[i, j] + m[i, j];
				}
			}
			return res;
		}

		private (int, int) CalcVec(int speed, double angle)
		{
			return new(
				(int)(speed * Math.Cos(angle)), 
				(int)(speed * Math.Sin(angle)) 
			);
		}

		private bool ContainsTwo(int[,] resArr)
		{
			return resArr.ToString().Contains("2");
		}

	}
}
