using System;
using System.Collections.Generic;

namespace Tanks
{
	public struct IntPair
	{
		public IntPair(int x, int y) { X = x; Y = y; }
		public int X { get; set; }
		public int Y { get; set; }

	}

	public class CollisisonAnalyserCommand : ICommand
	{
		int maxSpeed;
		List<int[,]> sprite1;
		List<int[,]> sprite2;

		public CollisisonAnalyserCommand(ICollisionAnalysable ian)
		{
			this.sprite1 = ian.GetFirstSprite();
			this.sprite2 = ian.GetSecondSprite();
			this.maxSpeed = IoC.Resolve<int>("GetMaxSpeed");
		}

		public void Execute()
		{
			List<string> res = new List<string>();
			foreach (var n in sprite1)
			{
				foreach (var m in sprite2)
				{
					foreach (string str in GetCollisions(n, m, sprite1.IndexOf(n), sprite1.IndexOf(m)))
					{
						res.Add(str);
					}
				}
			}
			IoC.Resolve<string>("Set collisions vectors", res);

		}

		private List<string> GetCollisions(int[,] n, int[,] m, int idxN, int idxM)
		{
			List<string> res = new List<string>();
			double angleN = GetAngleFromSprite(idxN, sprite1.Count);
			double angleM = GetAngleFromSprite(idxM, sprite2.Count);

			IntPair centerN = GetCenter(n);
			IntPair centerM = GetCenter(m);

			for (int ringRadius = 0;
				ringRadius < (maxSpeed + Math.Max(m.GetLength(0), m.GetLength(1)));
				ringRadius++)
			{
				IntPair centerOnRing = new IntPair(0, 0);
				for (centerOnRing.X = ringRadius; centerOnRing.X >= 0; centerOnRing.X--)
				{
					centerOnRing.Y = ringRadius - centerOnRing.X;
					
					IoC.Resolve<string>("Check Collision for coord", res, centerOnRing, angleN, angleM, n, m);
					IoC.Resolve<string>("Check Collision for coord", res, new IntPair(centerOnRing.X, (-1)*centerOnRing.Y), angleN, angleM, n, m);
					IoC.Resolve<string>("Check Collision for coord", res, new IntPair((-1)*centerOnRing.X, (-1)*centerOnRing.Y), angleN, angleM, n, m);
					IoC.Resolve<string>("Check Collision for coord", res, new IntPair((-1)*centerOnRing.X, centerOnRing.Y), angleN, angleM, n, m);

				}
			}



			return res;
		}

		private double GetAngleFromSprite(int idx, int length)
		{
			double res = (2 * Math.PI / length) * idx;
			return res;
		}

		private IntPair GetCenter(int[,] a)
		{
			IntPair res = new IntPair(0, 0);
			res.Y = a.GetLength(0) / 2;
			res.X = a.GetLength(1) / 2;
			return res;
		}

	}
}
