namespace Tanks
{
    public interface IUObject {
        object this[string prop] { get; }
        void setProp(string prop, object newValue);
        void deleteProp(string prop);
    }
}
