﻿using System;
using System.Collections.Concurrent;

namespace Tanks
{
    public class ThreadSoftStopCmd : ICommand
    {
        public ThreadSoftStopCmd(IThreadStopableSoft obj) 
        {

        }

        public void Execute() 
        {
            Action<BlockingCollection<ICommand>> newAct = (q) => 
            {
                ICommand cmd = null;
                try
                {
                    cmd = q.Take();
                    cmd.Execute();
                    if (q.Count<=0) 
                    {
                        IoC.Resolve<bool>("Thread.CanContinue.Setup",false);
                    }
                }
                catch (Exception ex)
                {
                    IoC.Resolve<ICommand>("ExHandler", cmd.GetType(), ex).Execute();
                }
            };

            IThreadProc.ProcAct = newAct;
        }
    }
}
