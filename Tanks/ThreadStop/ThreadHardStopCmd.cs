using System;

namespace Tanks
{
	public class ThreadHardStopCmd:ICommand
	{
		public ThreadHardStopCmd(IThreadStopableHard obj)
		{

		}
		public void Execute()
		{
			IoC.Resolve<bool>("Thread.CanContinue.Setup",false);
		}
	}
}
