using System;
using System.Collections.Generic;
using System.Collections;
using System.IO;

namespace Tanks
{
    public class MyEnumerator: IEnumerator<int[]>, IEnumerable<int[]> {
        StreamReader st;
        string fileName;
        string curString;
        
        public MyEnumerator (string fileName) {
            this.fileName=fileName;
            st=new StreamReader(fileName);
            curString="";
        }
        public bool MoveNext()
        {
            string line;
            if ((line = st.ReadLine()) != null) {
                curString = line;
                return true;
            } else {
                return false;
            }
        }
        public void Reset() {
            st = new StreamReader(fileName);
        }
        
        public void Dispose() {
            st.Close();
        }
        object IEnumerator.Current
        {
            get
            {
                return Current;
            }
        }
        public int[] Current
        {
            get
            {
                int[] rez = new int[4];
                int i = 0;
                string[] arr = curString.Split(' ');
                if (arr.Length!=4) {
                    throw new Exception("Invalid array length");
                }
                foreach (string el in arr) {
                    rez[i]=int.Parse(el.Trim());
                    i++;
                }
                return rez;
            }
        }
        public IEnumerator<int[]> GetEnumerator() {
            return this;
        }
        IEnumerator IEnumerable.GetEnumerator() {
            return this;
        }
    }

}
