using System.Collections;

namespace Tanks
{
    public class CreateDecisionTree:ICommand 
    {
        IDecisionTreeCreateable obj;
        public CreateDecisionTree(IDecisionTreeCreateable obj){
            this.obj = obj;
        }
        public void Execute() {
            IEnumerable lines = obj.Lines;

            var dep = "ColisionFor"+obj.Case+".Add";

            foreach(int[] line in lines) {
                IoC.Resolve<ICommand>(dep, line).Execute();
            }
        }
    }
}
