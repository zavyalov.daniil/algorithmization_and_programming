using System.Collections;
using System.Collections.Generic;

namespace Tanks
{
    public interface IDecisionTreeCreateable
    {
        IEnumerable<int[]> Lines {
            get;
        }
        string Case {
            get;
        }
    }
}
