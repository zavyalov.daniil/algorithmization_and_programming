using System.Collections.Generic;

namespace Tanks
{
    public interface IAppendVectorOfAttributes
    {
        int[] vector {
            get;
        }
        Dictionary<int, object> root {
            get;
            set;
        }
    }
}