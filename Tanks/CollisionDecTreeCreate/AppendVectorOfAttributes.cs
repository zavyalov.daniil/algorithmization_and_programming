using System.Numerics;
using System;
using System.Collections.Generic;

namespace Tanks
{
    public class AppendVectorOfAttributes:ICommand 
    {
        IAppendVectorOfAttributes obj;
        public AppendVectorOfAttributes(IAppendVectorOfAttributes sourse){
            if(sourse.vector.Length != 4)
                throw new Exception();
            this.obj = sourse;
        }
        public void Execute() {
            var cur = obj.root;
            for (int i=0; i<obj.vector.Length-1; i++) {
                cur.TryAdd(obj.vector[i], new Dictionary<int, object>());
                cur = (Dictionary<int, object>)cur[obj.vector[i]];
            }
            cur.TryAdd(obj.vector[obj.vector.Length-1], true);
        }
    }
}
