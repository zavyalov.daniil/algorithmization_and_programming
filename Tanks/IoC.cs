using System;

namespace Tanks
{
	public class IoC
	{
		public static T Resolve<T>(string key, params object[] args)
		{
			return (T)strategy(key, args);
		}



		private static Func<string, object[], object> strategy = (key, args) =>
		{
			if (key == "IoC.Setup")
			{
				var newStrategy = (Func<string, object[], object>)args[0];
				return new IoCSetupCommand(newStrategy);
			} else if (key == "IoC.Strategy")
			{
				return strategy;
			} else
			{
				throw new Exception("Unknown IoC KEY");
			}

		};

		public /*private*/ class IoCSetupCommand : ICommand
		{

			Func<string, object[], object> newStrategy;

			public IoCSetupCommand(Func<string, object[], object> newStrategy) { this.newStrategy = newStrategy; }

			public void Execute()
			{
				strategy = newStrategy;
			}
		}

	}


}
