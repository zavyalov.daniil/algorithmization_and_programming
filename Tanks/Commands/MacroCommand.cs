using System;
using System.Collections.Generic;
//using System.Linq;

namespace Tanks
{
	public class MacroCommand : ICommand
	{

		List<ICommand> commandsList;

		public MacroCommand(List<ICommand> incommingList)
		{
			this.commandsList = incommingList;
		}

		public MacroCommand()
		{
			commandsList = new List<ICommand>() { };
		}

		public void Execute()
		{
			commandsList.ForEach(x => x.Execute());
		}

		public int GetSize()
		{
			return commandsList.Count;
		}
	}
}
