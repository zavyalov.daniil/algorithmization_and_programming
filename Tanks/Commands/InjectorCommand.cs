using System;

namespace Tanks{
    public class InjectorCommand: ICommand {
        private ICommand macro;

        public InjectorCommand(ICommand macro){
            this.macro = macro;
        }

        public ICommand GetMacroCommand(){
            return macro;
        }
        public void SetMacroCommand(ICommand macro){
            this.macro = macro;
        }

        public void Execute(){
            macro.Execute();
        }
    }
}
