namespace Tanks
{
    public class MoveStopCommand:ICommand
    {
        IMoveStopable stopableObject; 
        
        public MoveStopCommand (IMoveStopable stopableObject) {
            this.stopableObject=stopableObject;
        }
        public void Execute() {
            stopableObject.ClearSpeed();
            stopableObject.ClearVelocity();
            stopableObject.GetMoveInjector().SetMacroCommand(new EmptyCommand());
        }
    }
}
