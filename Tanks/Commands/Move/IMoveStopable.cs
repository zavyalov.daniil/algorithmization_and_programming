using System;
namespace Tanks
{
    public interface IMoveStopable
    {
        public void ClearSpeed();
        public void ClearVelocity();
        public InjectorCommand GetMoveInjector();
    }
}
