namespace Tanks
{
    public class MoveCommand:ICommand
    {
        IMovable movable; 
        public MoveCommand (IMovable movable) {
            this.movable=movable;
        }
        public void Execute() {
            movable.SetPosition(movable.GetPosition()+movable.GetVelocity());
        }
    }
}
