using System;
using System.Collections.Generic;
namespace Tanks
{
    public class MoveStartCommand:ICommand
    {
        private IMoveStartable startableObject; 
        private uint speed;
        public MoveStartCommand (IMoveStartable startableObject, uint speed) {
            this.startableObject=startableObject;
            this.speed=speed;
        }
        public void Execute() {
            startableObject.SetSpeed(speed);
            startableObject.SetVelocity(new MyVector((int)(Math.Cos(startableObject.GetAngle())*(double)speed), (int)(Math.Sin(startableObject.GetAngle())*(double)speed)));

            ICommand moveMacroCmd = IoC.Resolve<ICommand>("MoveCommand", startableObject.GetSource()); 
            InjectorCommand moveInjector=new InjectorCommand(moveMacroCmd);

            startableObject.SetMoveInjector(moveInjector);
            ICommand pushCmd = IoC.Resolve<ICommand>("push", moveInjector);
            pushCmd.Execute();
        }
    }
}
