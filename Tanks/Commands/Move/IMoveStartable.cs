
using System;
namespace Tanks
{
    public interface IMoveStartable 
    {
        public void SetSpeed(uint speedMod);
        public void SetVelocity(MyVector velocity);
        public void SetMoveInjector(ICommand move);
        public double GetAngle();
        public IUObject GetSource();
    }
}
