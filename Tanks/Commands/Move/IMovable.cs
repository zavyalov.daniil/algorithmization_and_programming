namespace Tanks
{
    public interface IMovable
    {
        void SetPosition(MyVector newPosition);
        MyVector GetPosition();
        MyVector GetVelocity();
    }
}
