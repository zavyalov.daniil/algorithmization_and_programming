using System;

namespace Tanks {
	public class StopRotationCommand : ICommand {
		IStopableRotation rotableComponent;
		InjectorCommand inj;

		public StopRotationCommand(/*ref*/ IStopableRotation rotable,ref InjectorCommand injector) 
		{
			rotableComponent = rotable;
			inj = injector;
		}

		public void Execute() {
			inj.SetMacroCommand(new MacroCommand());
			rotableComponent.SetRortationVelocity(0);
		}
	}
 }
 