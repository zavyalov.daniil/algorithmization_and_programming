using System;

namespace Tanks {
	public interface IChangeVelocity : IRotable {
		public uint GetVelocityModule();

		public void SetVelocity(MyVector velocity);
	}
}
