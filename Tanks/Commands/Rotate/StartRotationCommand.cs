using System;
using System.Collections.Generic;

namespace Tanks
{
	public class StartRotateCommand : ICommand
	{
		IStartableRotation rotation;
		int rotationVelocity;
		InjectorCommand injector;

		public StartRotateCommand(/*ref*/ IStartableRotation rotation, int rotationVelocity)
		{
			this.rotation = rotation;
			this.rotationVelocity = rotationVelocity;
			
		}

		public void Execute()
		{
			rotation.SetRortationVelocity(this.rotationVelocity);
			ICommand macroCmd = IoC.Resolve<ICommand>("StartRotationMacro", rotation.GetSource());
			injector = new InjectorCommand(macroCmd);
			rotation.SetRotationInjector(injector);
			ICommand pushCmd = IoC.Resolve<ICommand>("push", injector);
			pushCmd.Execute();
		}
	}
}
