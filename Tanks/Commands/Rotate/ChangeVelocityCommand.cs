using System;

namespace Tanks
{
	public class ChangeVelocityCommand : ICommand
	{

		IChangeVelocity obj;

		public ChangeVelocityCommand(IChangeVelocity obj)
		{
			this.obj = obj;
		}
		public void Execute()
		{
			
			MyVector newVelocity = CalcVec(obj.GetAngle());
			obj.SetVelocity(newVelocity);
		}

		private MyVector CalcVec(double angle) {
			return new MyVector(
				(int)(obj.GetVelocityModule() * Math.Cos(angle)), 
				(int)(obj.GetVelocityModule() * Math.Sin(angle))
			);
		}
	}
}
