using System;

namespace Tanks {
	public interface IRotable {

		public void SetAngle(double a);
		public double GetAngle();

		public double GetRortationVelocity(); 
	}
}
