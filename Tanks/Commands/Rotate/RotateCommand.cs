using System;

namespace Tanks {
	public class RotateCommand : ICommand {
		IRotable objRotable;
		//double rotationVelocity;
		public RotateCommand(/*ref*/ IRotable objRotable) {
			this.objRotable = objRotable;
		}

		public void Execute(){
			objRotable.SetAngle((objRotable.GetAngle()+objRotable.GetRortationVelocity()));
		}
	}
}
