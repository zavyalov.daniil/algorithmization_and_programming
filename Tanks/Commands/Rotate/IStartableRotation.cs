using System;

namespace Tanks { 
	public interface IStartableRotation{
		public IUObject GetSource();
		public void SetRotationInjector(InjectorCommand rotation);
		public void SetRortationVelocity(double rotationVelocity); 
	}
}
