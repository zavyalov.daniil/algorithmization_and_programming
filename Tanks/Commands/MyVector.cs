using System;
namespace Tanks
{
    public class MyVector
    {
        private int x;
        private int y;
        public MyVector (int x, int y) {
            this.x=x;
            this.y=y;
        }
        int GetX () {
            return x;
        }
        int GetY () {
            return y;
        }
        double getMod () {
            return Math.Sqrt((double)(x*x+y*y));
        }
        public static MyVector operator +(MyVector c1, MyVector c2)
        {
            return new MyVector (c1.GetX()+c2.GetX(), c1.GetY()+c2.GetY());
        }
        public static bool operator ==(MyVector c1, MyVector c2)
        {
            return (c1.GetX()==c2.GetX()) & (c1.GetY()==c2.GetY());
        }
        public static bool operator !=(MyVector c1, MyVector c2)
        {
            return !((c1.GetX()==c2.GetX()) & (c1.GetY()==c2.GetY()));
        }
        public override bool Equals(Object obj)
        {
            if ((obj == null) || ! this.GetType().Equals(obj.GetType()))
            {
                return false;
            } else {
                MyVector p2 = (MyVector)obj;
                return (x==p2.GetX()) & (y==p2.GetY());
            }
        }
    }
}
