using System;

namespace Tanks
{
    public class ShootCommand : ICommand
    {
        IShootable shootable;
        public ShootCommand(IShootable shootable)
        {
            this.shootable = shootable;
        }

        public void Execute()
        {
            var obj = IoC.Resolve<IUObject>("CreateBullet");
            IoC.Resolve<ICommand>("SetPosition", obj, shootable.InitialBulletPosition).Execute();
            IoC.Resolve<ICommand>("SetDirection", obj, shootable.InitialBulletDirection).Execute();
            var action = IoC.Resolve<IUObject>("Action", obj, "Move", shootable.InitialVelocity);

            IoC.Resolve<ICommand>("StartMove", action).Execute();
        }
    }
}