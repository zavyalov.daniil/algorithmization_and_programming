using System;

namespace Tanks
{
    public interface IShootable
    {
        MyVector InitialVelocity {get;}
        MyVector InitialBulletPosition {get;}
        int InitialBulletDirection {get;}
    }
}