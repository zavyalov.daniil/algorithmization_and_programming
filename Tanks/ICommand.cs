﻿namespace Tanks
{
	public interface ICommand
	{
		public void Execute();
	}
}
