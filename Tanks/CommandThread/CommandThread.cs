using System;
using System.Collections.Concurrent;

namespace Tanks
{ 
    public class CmdThread {
        private static Action<BlockingCollection<ICommand>> procAct = (commandQueue) => {
            ICommand cmd = null;
            try {
                cmd = commandQueue.Take();
                cmd.Execute();      
            } catch (Exception ex) {
                IoC.Resolve<ICommand>("ExHandler", cmd.GetType(), ex).Execute();
            }
        };

        public static Action<BlockingCollection<ICommand>> ProcAct {
            get {
                return procAct;
            }
            set {
                procAct = value; 
            }
        }
        
        public static void ThreadProc() {
            BlockingCollection<ICommand> q = IoC.Resolve<BlockingCollection<ICommand>>("Thread.CommandQueue");
            while(IoC.Resolve<bool>("Thread.CanContinue")) {
                procAct(q);
            }
        }
    }
}
