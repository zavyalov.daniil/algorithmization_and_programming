using System;
namespace Tanks
{
    public interface IMatchable
    {
        public string Type { get;}
        public int[] Attributes { get;}
    }
}
