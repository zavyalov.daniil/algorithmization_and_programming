using System;
using System.Collections;
using System.Collections.Generic;

namespace Tanks
{
    public class CollisionMatcher
    {
        Dictionary<int, object> tree;
        public CollisionMatcher(Dictionary<int, object> tree)
        {
            this.tree = tree;
        }

        public void Match (int[] attributes, ICommand onSuccess, ICommand onFail) 
        {
            object temp_dict = (Dictionary<int,object>)tree;

            foreach(int i in attributes)
            {
                if (!((Dictionary<int,object>)temp_dict).TryGetValue(i, out temp_dict))
                {
                    onFail.Execute();
                    return;
                }
            }

            onSuccess.Execute();
        }   
    }
}