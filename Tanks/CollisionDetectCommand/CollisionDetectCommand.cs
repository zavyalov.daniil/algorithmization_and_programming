using System;
using System.Collections;
using System.Collections.Generic;

namespace Tanks
{
    public class CollisionDetectCommand : ICommand
    {
        public int[] attributes = new int[4];
        IMatchable obj1, obj2;
        ICommand onSuccess, onFail;
        public CollisionDetectCommand(IMatchable obj1, IMatchable obj2, ICommand onSuccess, ICommand onFail)
        {
            this.obj1 = obj1;
            this.obj2 = obj2;
            this.onSuccess = onSuccess;
            this.onFail = onFail;

            attributes[0] = Math.Abs(obj1.Attributes[0] - obj2.Attributes[0]);
            attributes[1] = Math.Abs(obj1.Attributes[1] - obj2.Attributes[1]);
            attributes[2] = Math.Abs(obj1.Attributes[2] - obj2.Attributes[2]);
            attributes[3] = Math.Abs(obj1.Attributes[3] - obj2.Attributes[3]);
        }
        public void Execute()
        {
            var dep = "GetDecisionTreeFor" + obj1.Type + "And" + obj2.Type;
            Dictionary<int, object> tree = IoC.Resolve<Dictionary<int, object>>(dep);
            CollisionMatcher matcher = new CollisionMatcher(tree);
            matcher.Match(attributes, onSuccess, onFail);
        }
    }
}