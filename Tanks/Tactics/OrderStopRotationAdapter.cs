using System;

namespace Tanks
{
	public class OrderStopRotationAdapter : IIoCTactics
	{
		public T Exequte<T>(params object[] args)
		{
			IUObject order = (IUObject)args[0];
			return (T)IoC.Resolve<ICommand>("GetStopRotationCommand", order["IStopableRotation"]);
		}
	}
}
