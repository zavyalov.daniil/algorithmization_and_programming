using System;
using System.Collections.Generic;

namespace Tanks
{
	public class OrdersInterpretationTactics: IIoCTactics
	{
		string orderType;
		IUObject order;

		private Dictionary<string, IIoCTactics> adaptTactics;

		public T Exequte<T>(params object[] args)
		{
			adaptTactics = IoC.Resolve<Dictionary<string, IIoCTactics>>("Orders.InterpritationTactics.GetCollection");
			this.orderType = (string)args[0];
			this.order = (IUObject)args[1];
			IIoCTactics tactics = (IIoCTactics)adaptTactics[orderType];
			ICommand resultCmd = (ICommand)(tactics.Exequte<ICommand>(order));
			return (T)resultCmd;
		}
	}
}
