using System;

namespace Tanks
{
	public class OrderShootAdapter : IIoCTactics
	{
		public T Exequte<T>(params object[] args)
		{
			IUObject order = (IUObject)args[0];
			return (T)IoC.Resolve<ICommand>("GetShootCommand", order["IShootable"]);
		}
	}
}
