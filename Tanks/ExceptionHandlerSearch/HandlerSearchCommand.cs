using System;
using System.Collections.Generic;

namespace Tanks
{
	public class HandlerSearchCommand:ICommand
	{
		Type sourceExceptionType;
		Type sourceObjectType;
		object[] actionInputArray;
		public HandlerSearchCommand(IHandlerSearchable input)
		{
			sourceExceptionType = input.GetSourceException();
			sourceObjectType = input.GetSourceObject();
			actionInputArray = input.GetActionInputArray();
		}
		public void Execute()
		{
			var tree = IoC.Resolve<Dictionary<Type,Func<object[],Action<object[]> > > >("ExceptionHandler.GetTree");
			tree[sourceObjectType](new object[]{sourceExceptionType,sourceObjectType})(actionInputArray);
		}
	}
}
