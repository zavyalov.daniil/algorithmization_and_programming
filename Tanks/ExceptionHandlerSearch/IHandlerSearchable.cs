using System;

namespace Tanks
{
	public interface IHandlerSearchable
	{
		public Type GetSourceException();
		public Type GetSourceObject();
		public object[] GetActionInputArray();
	}
}
