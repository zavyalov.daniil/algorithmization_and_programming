using System;
using System.Collections.Generic;

namespace Tanks
{
    public class ExceptionHandlerRegister2:ICommand {
        private IExceptionRegister2 obj;
        public ExceptionHandlerRegister2(IExceptionRegister2 obj) {
            this.obj = obj;
        }
        public void Execute() {
            Dictionary<Type, Func<object[], Action<object[]>>> tree = IoC.Resolve<Dictionary<Type, Func<object[], Action<object[]>>>>("ExceptionHandler.GetTree");
            Dictionary<Type, Action<object[]>> secondLevelTree = IoC.Resolve<Dictionary<Type, Action<object[]>>>("ExceptionHandler.GetSecLevelTree", obj.ObjType);
            secondLevelTree[obj.Ex.GetType()] = obj.Act;
            Func<object[], Action<object[]>> f = (objects) => 
            { 
                return IoC.Resolve<Dictionary<Type, Action<object[]>>>("ExceptionHandler.GetSecLevelTree", objects[1])[(Type)(objects[0])];
            };
            tree[obj.ObjType] = f;
        }
    }
}