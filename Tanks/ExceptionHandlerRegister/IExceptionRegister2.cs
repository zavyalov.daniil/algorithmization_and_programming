using System;
using System.Collections.Generic;

namespace Tanks
{
    public interface IExceptionRegister2
    {
        Type ObjType { get; }
        Exception Ex { get; }
        Action<object[]> Act { get; }
    }
}
