using System;
using System.Collections.Generic;

namespace Tanks
{
    public interface IExceptionRegister
    {
        Type ObjType { get; }
        Action<object[]> Act { get; }
    }
}
