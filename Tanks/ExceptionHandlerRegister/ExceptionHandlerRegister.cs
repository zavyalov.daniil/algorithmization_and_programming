using System;
using System.Collections.Generic;

namespace Tanks
{
    public class ExceptionHandlerRegister:ICommand {
        private IExceptionRegister obj;
        public ExceptionHandlerRegister(IExceptionRegister obj) {
            this.obj = obj;
        }
        public void Execute() {
            Dictionary<Type, Func<object[], Action<object[]>>> tree = IoC.Resolve<Dictionary<Type, Func<object[], Action<object[]>>>>("ExceptionHandler.GetTree");
            Func<object[], Action<object[]>> f = (objects) => { return obj.Act; };
            tree[obj.ObjType] = f;
        }
    }
}


