using System;
using Xunit;
using Moq;
using System.Collections.Concurrent;
using System.Threading;
using System.Collections.Generic;
using System.Collections;
using System.IO;

namespace Tanks.Test
{
    public class CommThreadTest
    {
        [Fact]
        public void TestOrderHandlerCmd()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
            try
            {
                var interpretCmd = new Mock<ICommand>();
                interpretCmd.Setup(m => m.Execute()).Verifiable();
                
                Func<string, object[], object> strategy = (key, args) =>
                {
                    if ("OrderInterpretCmd" == key)
                    {
                        return interpretCmd.Object;
                    }
                    else if ("IoC.Setup" == key)
                    {
                        var newStrategy = (Func<string, object[], object>)args[0];
                        return new IoC.IoCSetupCommand(newStrategy);
                    }
                    else
                    {
                        throw new Exception();
                    }
                };
                IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

                var uObject = new Mock<IUObject>();
                ConcurrentQueue<IUObject> q = new ConcurrentQueue<IUObject>();
                q.Enqueue(uObject.Object);
                OrderHandlerCmd handler = new OrderHandlerCmd(q);
                handler.Execute();

                interpretCmd.VerifyAll();
                
            }
            finally
            {
                IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
            }
        }

        [Fact]
        public void TestOrderHandlerOrderQueueEmpty()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
            try
            {
                bool executed = false;
                var interpretCmd = new Mock<ICommand>();
                interpretCmd.Setup(m => m.Execute()).Callback(() => executed = true).Verifiable();

                Func<string, object[], object> strategy = (key, args) =>
                {
                    if ("OrderInterpretCmd" == key)
                    {
                        return interpretCmd.Object;
                    }
                    else if ("IoC.Setup" == key)
                    {
                        var newStrategy = (Func<string, object[], object>)args[0];
                        return new IoC.IoCSetupCommand(newStrategy);
                    }
                    else
                    {
                        throw new Exception();
                    }
                };
                IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

                ConcurrentQueue<IUObject> q = new ConcurrentQueue<IUObject>();
                OrderHandlerCmd handler = new OrderHandlerCmd(q);
                handler.Execute();

                Assert.False(executed);

            }
            finally
            {
                IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
            }
        }

        [Fact]
        public void TestOrderInterpretCmd()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
            try
            {
                var adapter = new Mock<IOrderInterpretable> ();
                adapter.Setup(m => m.OrderType).Returns("Move").Verifiable();
                var uObject = new Mock<IUObject>();
                adapter.Setup(m => m.Order).Returns(uObject.Object).Verifiable();

                BlockingCollection<ICommand> commandQueue = new BlockingCollection<ICommand>();

                var cmd = new Mock<ICommand>();
                cmd.Setup(m => m.Execute()).Verifiable();

                Func<string, object[], object> strategy = (key, args) =>
                {
                    if ("Thread.CommandQueue" == key)
                    {
                        return commandQueue;
                    }
                    else if("Orders.GetCommand" == key)
                    {
                        return cmd.Object;
                    }
                    else if ("IoC.Setup" == key)
                    {
                        var newStrategy = (Func<string, object[], object>)args[0];
                        return new IoC.IoCSetupCommand(newStrategy);
                    }
                    else
                    {
                        throw new Exception();
                    }
                };
                IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

                OrderInterpretCmd interpretCmd = new OrderInterpretCmd(adapter.Object);
                interpretCmd.Execute();

                ICommand c;
                if (commandQueue.TryTake(out c)) {
                    c.Execute();
                }
                cmd.VerifyAll();
            }
            finally
            {
                IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
            }
        }

        [Fact]
        public void TestTacticsOrderInterpretCreate()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
            try
            {
                var adapter = new Mock<IOrderInterpretable>();
                adapter.Setup(m => m.OrderType).Returns("Move").Verifiable();
                var uObject = new Mock<IUObject>();
                adapter.Setup(m => m.Order).Returns(uObject.Object).Verifiable();

                BlockingCollection<ICommand> commandQueue = new BlockingCollection<ICommand>();

                var cmd = new Mock<ICommand>();
                cmd.Setup(m => m.Execute()).Verifiable();

                Func<string, object[], object> strategy = (key, args) =>
                {
                    if ("Adapter.IOrderInterpretable.Create" == key)
                    {
                        return adapter.Object;
                    }
                    else if("Thread.CommandQueue" == key)
                    {
                        return commandQueue;
                    }
                    else if ("Orders.GetCommand" == key)
                    {
                        return cmd.Object;
                    }
                    else if ("IoC.Setup" == key)
                    {
                        var newStrategy = (Func<string, object[], object>)args[0];
                        return new IoC.IoCSetupCommand(newStrategy);
                    }
                    else
                    {
                        throw new Exception();
                    }
                };
                IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

                TacticsOrderInterpretCreate orderInterpretCreate = new TacticsOrderInterpretCreate();
                ICommand interpretCmd = orderInterpretCreate.Execute(uObject.Object);
                interpretCmd.Execute();

                ICommand c;
                if (commandQueue.TryTake(out c))
                {
                    c.Execute();
                }
                cmd.VerifyAll();
            }
            finally
            {
                IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
            }
        }

        [Fact]
        public void TestTacticsOrderInterpretCreateObj()
        {
            var uObject = new Mock<IUObject>();
            uObject.Setup(m => m["Type"]).Returns("Move").Verifiable();
            TacticsOrderInterpretCreateObj cmd = new TacticsOrderInterpretCreateObj();
            IOrderInterpretable obj = cmd.Execute(uObject.Object);

            Assert.Equal("Move", obj.OrderType);
        }




        [Fact]
        public void ThreadExecutingCommandsFromQueue()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
            try {
                var cmd1 = new Mock<ICommand>();
                cmd1.Setup(m => m.Execute()).Verifiable();
                var cmd2 = new Mock<ICommand>();
                cmd2.Setup(m => m.Execute()).Verifiable();
                var cmd3 = new Mock<ICommand>();
                cmd3.Setup(m => m.Execute()).Callback(() => IoC.Resolve<bool>("Thread.CanContinue.Set", false)).Verifiable();

                Thread t = new Thread(new ThreadStart(CmdThread.ThreadProc));
                bool canContinue = true;
                var q = new BlockingCollection<ICommand>();
                Func<string, object[], object> strategy = (key, args) =>
                {
                    if ("Thread.CommandQueue" == key) {
                        q.Add(cmd1.Object);
                        q.Add(cmd2.Object);
                        q.Add(cmd3.Object);
                        return q;
                    }
                    else if ("Thread.CanContinue" == key)
                    {
                        return canContinue;
                    }
                    else if ("Thread.CanContinue.Set" == key)
                    {
                        canContinue = (bool)args[0];
                        return canContinue;
                    }
                    else if ("IoC.Setup" == key)
                    {
                        var newStrategy = (Func<string, object[], object>) args[0];
                        return new IoC.IoCSetupCommand(newStrategy);
                    }
                    else
                    {
                        throw new Exception();
                    }
                };
                IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

                t.Start();
                Assert.True(t.Join(5000));

                cmd1.VerifyAll();
                cmd2.VerifyAll();
                cmd3.VerifyAll();
                q.Dispose();
            }
            finally {
                IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
            }
        }

        [Fact]
        public void ThreadAddingCommandAtRuntime()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
            try {
                ManualResetEvent canContinueTest = new ManualResetEvent(false);
                var cmd1 = new Mock<ICommand>();
                cmd1.Setup(m => m.Execute()).Verifiable();
                var cmd2 = new Mock<ICommand>();
                cmd2.Setup(m => m.Execute()).Callback(() => canContinueTest.Set()).Verifiable();
                var cmd3 = new Mock<ICommand>();
                cmd3.Setup(m => m.Execute()).Callback(() => IoC.Resolve<bool>("Thread.CanContinue.Set", false)).Verifiable();

                Thread t = new Thread(new ThreadStart(CmdThread.ThreadProc));
                bool canContinue = true;
                var q = new BlockingCollection<ICommand>();
                Func<string, object[], object> strategy = (key, args) =>
                {
                    if ("Thread.CommandQueue" == key) {
                        q.Add(cmd1.Object);
                        q.Add(cmd2.Object);
                        return q;
                    }
                    else if ("Thread.CanContinue" == key)
                    {
                        return canContinue;
                    }
                    else if ("Thread.CanContinue.Set" == key)
                    {
                        canContinue = (bool)args[0];
                        return canContinue;
                    }
                    else if ("IoC.Setup" == key)
                    {
                        var newStrategy = (Func<string, object[], object>) args[0];
                        return new IoC.IoCSetupCommand(newStrategy);
                    }
                    else
                    {
                        throw new Exception();
                    }
                };
                IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

                t.Start();

                Assert.True(canContinueTest.WaitOne(5000));
                q.Add(cmd3.Object);

                Assert.True(t.Join(5000));

                cmd1.VerifyAll();
                cmd2.VerifyAll();
                cmd3.VerifyAll();
                q.Dispose();
            }
            finally {
                IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
            }
        }

        [Fact]
        public void ThreadHandlingExceptions()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
            try {
                var cmd1 = new Mock<ICommand>();
                cmd1.Setup(m => m.Execute()).Verifiable();
                var cmd2 = new Mock<ICommand>();
                cmd2.Setup(m => m.Execute()).Callback( () => 
                { 
                    throw new ArgumentException(); 
                }).Verifiable();
                var cmd3 = new Mock<ICommand>();
                cmd3.Setup(m => m.Execute()).Callback(() => IoC.Resolve<bool>("Thread.CanContinue.Set", false)).Verifiable();
                var handlerCmd = new Mock<ICommand>();
                handlerCmd.Setup(m => m.Execute()).Verifiable();

                Thread t = new Thread(new ThreadStart(CmdThread.ThreadProc));
                bool canContinue = true;
                var q = new BlockingCollection<ICommand>();
                Func<string, object[], object> strategy = (key, args) =>
                {
                    if ("Thread.CommandQueue" == key) {
                        q.Add(cmd1.Object);
                        q.Add(cmd2.Object);
                        q.Add(cmd3.Object);
                        return q;
                    }
                    else if ("Thread.CanContinue" == key)
                    {
                        return canContinue;
                    }
                    else if ("Thread.CanContinue.Set" == key)
                    {
                        canContinue = (bool)args[0];
                        return canContinue;
                    }
                    else if ("ExHandler" == key)
                    {
                        
                        return handlerCmd.Object;
                    }
                    else if ("IoC.Setup" == key)
                    {
                        var newStrategy = (Func<string, object[], object>) args[0];
                        return new IoC.IoCSetupCommand(newStrategy);
                    }
                    else
                    {
                        throw new Exception();
                    }
                };
                IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

                t.Start();
                Assert.True(t.Join(5000));

                cmd1.VerifyAll();
                cmd2.VerifyAll();
                cmd3.VerifyAll();
                handlerCmd.VerifyAll();
                q.Dispose();
            }
            finally {
                IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
            }
        }

        [Fact]
        public void ThreadChangeProcessAction()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
            try
            {
                bool actionChanged = false;
                Action<BlockingCollection<ICommand>> newProcAct = (commandQueue) => {
                    ICommand cmd = null;
                    try
                    {
                        actionChanged = true;
                        cmd = commandQueue.Take();
                        cmd.Execute();
                    }
                    catch (Exception ex)
                    {
                        IoC.Resolve<ICommand>("ExHandler", cmd.GetType(), ex).Execute();
                    }
                };

                var cmd1 = new Mock<ICommand>();
                cmd1.Setup(m => m.Execute()).Callback(() => CmdThread.ProcAct = newProcAct).Verifiable();
                var cmd2 = new Mock<ICommand>();
                cmd2.Setup(m => m.Execute()).Callback(() => IoC.Resolve<bool>("Thread.CanContinue.Set", false)).Verifiable();

                Thread t = new Thread(new ThreadStart(CmdThread.ThreadProc));
                bool canContinue = true;
                var q = new BlockingCollection<ICommand>();
                Func<string, object[], object> strategy = (key, args) =>
                {
                    if ("Thread.CommandQueue" == key)
                    {
                        q.Add(cmd1.Object);
                        q.Add(cmd2.Object);
                        return q;
                    }
                    else if ("Thread.CanContinue" == key)
                    {
                        return canContinue;
                    }
                    else if ("Thread.CanContinue.Set" == key)
                    {
                        canContinue = (bool)args[0];
                        return canContinue;
                    }
                    else if ("IoC.Setup" == key)
                    {
                        var newStrategy = (Func<string, object[], object>)args[0];
                        return new IoC.IoCSetupCommand(newStrategy);
                    }
                    else
                    {
                        throw new Exception();
                    }
                };
                IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

                t.Start();
                t.Join(5000);
                Assert.True(actionChanged);

                cmd1.VerifyAll();
                cmd2.VerifyAll();
                q.Dispose();
            }
            finally
            {
                IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
            }
        }

        [Fact]
        public void ThreadGetProcessAction()
        { 
            var cmd1 = new Mock<ICommand>();
            cmd1.Setup(m => m.Execute()).Verifiable();

            var q = new BlockingCollection<ICommand>();
            q.Add(cmd1.Object);

            CmdThread.ProcAct(q);

            cmd1.VerifyAll();
            q.Dispose();
        }

        //старые тесты

        [Fact]
        public void ExceptionHandlerRegisterTest1()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

            var m = new Mock<IExceptionRegister>();
            var commandMock = (new Mock<ICommand>()).Object;
            m.Setup(m => m.ObjType).Returns(commandMock.GetType()).Verifiable();
            bool handled = false;
            m.Setup(m => m.Act).Returns((objects) => handled = true).Verifiable();

            Dictionary<Type, Func<object[], Action<object[]>>> tree = new Dictionary<Type, Func<object[], Action<object[]>>>() {};
            Func<string, object[], object> strategy = (key, args) =>
            {
                if ("ExceptionHandler.GetTree"==key) {
                    return tree;
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };
            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

            var c = new ExceptionHandlerRegister(m.Object);
            c.Execute();

            tree[commandMock.GetType()](new object[]{})(new object[]{});
            Assert.True(handled);

            IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
        }

        [Fact]
        public void ExceptionHandlerRegisterOverwriting()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

            var m = new Mock<IExceptionRegister>();
            var commandMock = (new Mock<ICommand>()).Object;
            m.Setup(m => m.ObjType).Returns(commandMock.GetType()).Verifiable();
            bool handled = false;
            m.Setup(m => m.Act).Returns((objects) => handled = true).Verifiable();

            Dictionary<Type, Func<object[], Action<object[]>>> tree = new Dictionary<Type, Func<object[], Action<object[]>>>() 
            {
                { commandMock.GetType(), (objects) => { return (objects) => handled = false; } }
            };
            Func<string, object[], object> strategy = (key, args) =>
            {
                if ("ExceptionHandler.GetTree"==key) {
                    return tree;
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };
            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

            var c = new ExceptionHandlerRegister(m.Object);
            c.Execute();

            tree[commandMock.GetType()](new object[]{})(new object[]{});
            Assert.True(handled);

            IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
        }

        [Fact]
        public void ExceptionHandlerRegister2Test1()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

            var m = new Mock<IExceptionRegister2>();
            var commandMock = (new Mock<ICommand>()).Object;
            m.Setup(m => m.ObjType).Returns(commandMock.GetType()).Verifiable();
            m.Setup(m => m.Ex).Returns(new Exception("TestEx")).Verifiable();
            bool handled = false;
            m.Setup(m => m.Act).Returns((objects) => handled = true).Verifiable();

            Dictionary<Type, Func<object[], Action<object[]>>> tree = new Dictionary<Type, Func<object[], Action<object[]>>>() {};
            Dictionary<Type, Action<object[]>> secondLevelTree = new Dictionary<Type, Action<object[]>>() {};
            Func<string, object[], object> strategy = (key, args) =>
            {
                if ("ExceptionHandler.GetTree"==key) {
                    return tree;
                }
                else if ("ExceptionHandler.GetSecLevelTree" == key)
                {
                    return secondLevelTree;
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };
            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

            var c = new ExceptionHandlerRegister2(m.Object);
            c.Execute();
            tree[commandMock.GetType()](new object[] { (new Exception("TestEx")).GetType(), commandMock.GetType() })(new object[]{});

            Assert.True(handled);

            IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
        }

        /// 
        /// Тесты их прошлых задач
        /// 
        [Fact]
        public void CreateDecisionTreeTest()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

            var m = new Mock<IDecisionTreeCreateable>();
            m.Setup(m => m.Case).Returns("TankToTank").Verifiable();
            m.Setup(m => m.Lines).Returns(new List<int[]> { new int[] { 1, 2, 3, 4}, new int[] { 1, 2, 3, 5} }).Verifiable();

            var cmd1 = new Mock<ICommand>();
            cmd1.Setup(m => m.Execute()).Verifiable();

            Func<string, object[], object> strategy = (key, args) =>
            {
                if ("ColisionForTankToTank.Add"==key) {
                    return cmd1.Object;
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };
            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();
            
            var c = new CreateDecisionTree(m.Object);

            c.Execute();
            m.VerifyAll();
            cmd1.VerifyAll();
            IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
        }

        [Fact]
        public void AppendVectorOfAttributesTest()
        {
            var m = new Mock<IAppendVectorOfAttributes>();
            m.SetupGet(m => m.vector).Returns(new int[] { 1, 2, 3, 5 }).Verifiable();
            m.SetupGet(m => m.root).Returns(new Dictionary<int, object>(){}).Verifiable();
            m.SetupSet(m => m.root = It.Is<Dictionary<int, object>> 
            (
                d => d.Equals
                    (
                    new Dictionary<int, object>  
                    { { 1, new Dictionary<int, object> 
                        { { 2, new Dictionary<int, object> 
                            { { 3, new Dictionary<int, object> 
                                { { 5, true } } 
                            } } 
                        } }
                    } }
                )
            )
                
            ).Verifiable();

            var c = new AppendVectorOfAttributes(m.Object);

            c.Execute();

            Assert.Equal(true, ((Dictionary<int, object>)((Dictionary<int, object>)((Dictionary<int, object>)(Dictionary<int, object>)m.Object.root[1])[2])[3])[5]);
        }
        [Fact]
        public void AppendVectorOfAttributesIncorrectLength()
        {
            var m = new Mock<IAppendVectorOfAttributes>();
            m.SetupGet(m => m.vector).Returns(new int[] { 1, 2, 3 }).Verifiable();
            m.Setup(m => m.root).Returns(new Dictionary<int, object>(){}).Verifiable();
 
            Assert.Throws<Exception>(()=>new AppendVectorOfAttributes(m.Object));
            
        }
        [Fact]
        public void MyEnumeratorTest()
        {
            string testFileName="TestFile.txt";
            using (StreamWriter sw = File.CreateText(testFileName))
            {
                sw.WriteLine("1 2 3 4");
                sw.WriteLine("2 3 4 5");
            }
            
            MyEnumerator enumerator = new MyEnumerator(testFileName);
            int n=0;
            foreach(int[] i in enumerator) {
                Assert.Equal(4, i.Length);
                Assert.Equal(1+n, i[0]);
                Assert.Equal(2+n, i[1]);
                Assert.Equal(3+n, i[2]);
                Assert.Equal(4+n, i[3]);
                n++;
            }
            Assert.Equal(2, n);
            enumerator.Dispose();
            File.Delete(testFileName);
        }
        [Fact]
        public void MyEnumeratorTestEx()
        {
            string testFileName2="TestFile2.txt";
            using (StreamWriter sw = File.CreateText(testFileName2))
            {
                sw.WriteLine("1 2 3 4");
                sw.WriteLine("2 3 4");
            }
            
            MyEnumerator enumerator = new MyEnumerator(testFileName2);
            try { 
                foreach(int[] i in enumerator) {}
            } catch(Exception ex){
                Assert.Equal("Invalid array length", ex.Message);
            }
            enumerator.Dispose();
            File.Delete(testFileName2);
        }
        [Fact]
        public void MyEnumeratorTestMethods()
        {
            string testFileName="TestFile3.txt";
            using (StreamWriter sw = File.CreateText(testFileName))
            {
                sw.WriteLine("1 2 3 4");
                sw.WriteLine("2 3 4 5");
            }
            
            IEnumerable enumerator = new MyEnumerator(testFileName);
            int n=0;
            foreach(int[] i in enumerator) {
                Assert.Equal(4, i.Length);
                Assert.Equal(4+n, i[3]);
                n++;
            }
            ((IEnumerator)enumerator).Reset();
            n=0;
            foreach(int[] i in enumerator) {
                Assert.Equal(4, i.Length);
                Assert.Equal(4+n, i[3]);
                n++;
            }
            File.Delete(testFileName);
            
        }
        [Fact]
        public void TestIoCEx()
        {
            Assert.Throws<Exception>(()=>IoC.Resolve<Object>("Text"));
        }

        [Fact]
		public void TestExecute_NoCollisison()
		{
			var defaultStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
			{
				if (key == "GetMaxSpeed")
				{
					return (int)4;
				}
				else if (key == "IoC.Strategy")
				{
					return defaultStrategy;
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else
				{
					throw new Exception("Unknown IoC KEY");
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();
			try
			{
				Mock<IGetCollisison> iGetCollisionMock = new Mock<IGetCollisison>();
				iGetCollisionMock.Setup(f => f.GetCenterCoords()).Returns(new IntPair(10, 0)).Verifiable();
				iGetCollisionMock.Setup(f => f.GetFirstDirection()).Returns(Math.PI).Verifiable();
				iGetCollisionMock.Setup(f => f.GetSecondDirection()).Returns(Math.PI).Verifiable();
				int[,] n = new int[,]
				{
				{1,1},
				{1,1}
				};
				var m = new int[,]
				{
				{1,1},
				{1,1}
				};
				iGetCollisionMock.Setup(f => f.GetFirstArray()).Returns(n).Verifiable();
				iGetCollisionMock.Setup(f => f.GetSecondArray()).Returns(m).Verifiable();

				List<string> res = new List<string> { "1 2 3 4" };
				iGetCollisionMock.Setup(f => f.GetResList()).Returns(res).Verifiable();



				GetCollisionCommand cmd = new GetCollisionCommand(iGetCollisionMock.Object);
				cmd.Execute();

				List<string> preRes = new List<string> { "1 2 3 4" };

				iGetCollisionMock.VerifyAll();
				Assert.Equal<List<string>>(preRes, res);
			}
			finally
			{
				IoC.Resolve<ICommand>("IoC.Setup", defaultStrategy).Execute();
			}


		}


		[Fact]
		public void TestExecute_Some_Collisison()
		{
			var defaultStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
			{
				if (key == "GetMaxSpeed")
				{
					return (int)4;
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else if (key == "IoC.Strategy")
				{
					return defaultStrategy;
				}
				else
				{
					throw new Exception("Unknown IoC KEY");
				}

			};
			try
			{
				IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

				Mock<IGetCollisison> iGetCollisionMock = new Mock<IGetCollisison>();
				iGetCollisionMock.Setup(f => f.GetCenterCoords()).Returns(new IntPair(0, 0)).Verifiable();
				iGetCollisionMock.Setup(f => f.GetFirstDirection()).Returns(Math.PI).Verifiable();
				iGetCollisionMock.Setup(f => f.GetSecondDirection()).Returns(Math.PI).Verifiable();
				int[,] n = new int[,]
				{
				{1,1},
				{1,1}
				};
				var m = new int[,]
				{
				{1,1},
				{1,1}
				};
				iGetCollisionMock.Setup(f => f.GetFirstArray()).Returns(n).Verifiable();
				iGetCollisionMock.Setup(f => f.GetSecondArray()).Returns(m).Verifiable();

				List<string> res = new List<string> { "1 2 3 4" };
				iGetCollisionMock.Setup(f => f.GetResList()).Returns(res).Verifiable();



				GetCollisionCommand cmd = new GetCollisionCommand(iGetCollisionMock.Object);
				cmd.Execute();

				List<string> preRes = new List<string> { "1 2 3 4" };

				iGetCollisionMock.VerifyAll();
				Assert.True(preRes.Count < res.Count);
			}
			finally
			{
				IoC.Resolve<ICommand>("IoC.Setup", defaultStrategy).Execute();
			}

		}


		[Fact]
		public void CheckCollisisons_Test()
		{
			var defaultStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
			{
				if (key == "GetMaxSpeed")
				{
					return (int)5;
				}
				else if (key == "Check Collision for coord")
				{
					((List<string>)args[0]).Add("1 2 3 4");
					return "string";
				}
				else if (key == "Set collisions vectors")
				{
					return "succesfull";
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else if (key == "IoC.Strategy")
				{
					return defaultStrategy;
				}
				else
				{
					throw new Exception("Unknown IoC KEY");
				}

			};
			try
			{
				IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

				int[,] arr = {
				{1}
			};
				List<int[,]> sprite1 = new List<int[,]> { arr };
				List<int[,]> sprite2 = new List<int[,]> { arr };

				Mock<ICollisionAnalysable> ica = new Mock<ICollisionAnalysable>();
				ica.Setup(f => f.GetFirstSprite()).Returns(sprite1).Verifiable();
				ica.Setup(f => f.GetSecondSprite()).Returns(sprite2).Verifiable();

				CollisisonAnalyserCommand cmd = new CollisisonAnalyserCommand(ica.Object);
				cmd.Execute();

				ica.VerifyAll();
			}
			finally
			{
				IoC.Resolve<ICommand>("IoC.Setup", defaultStrategy).Execute();
			}

		}
		[Fact]
		public void FirstLvlSearch()
		{
			bool handled = false;
			
			Mock<IHandlerSearchable> searcherInterface = new Mock<IHandlerSearchable>();
			Action<object[]> resultAction = (m)=>{};
			searcherInterface.Setup(m=> m.GetSourceException()).Returns(typeof(Exception)).Verifiable();
			searcherInterface.Setup(m=> m.GetSourceObject()).Returns(typeof(ICommand)).Verifiable();
			searcherInterface.Setup(m=> m.GetActionInputArray()).Returns(new object[]{}).Verifiable();

			Dictionary<Type, Func<object[], Action<object[]>>> decisionTree = new Dictionary<Type, Func<object[], Action<object[]>>>();

			Action<object[]> act = (m) => { handled = true; };

			Func<object[], Action<object[]>> foo = delegate (object[] param)
				{
					return act;
				};

			decisionTree.Add(typeof(ICommand), foo);

			//ioc conf
			var defaultStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
			{
				if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else if (key == "ExceptionHandler.GetTree")
				{
					return decisionTree;
				}
				else
				{
					throw new Exception("Unknown IoC KEY");
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			//test
			
			HandlerSearchCommand searchCommand = new HandlerSearchCommand(searcherInterface.Object);
			searchCommand.Execute();

			searcherInterface.VerifyAll();
			Assert.True(handled);
			//test ends

			//ioc
			IoC.Resolve<ICommand>("IoC.Setup", defaultStrategy).Execute();
		}

		[Fact]
		public void SecondLvlSearch()
		{
			bool handled = false;
			
			var searcherInterface = new Mock<IHandlerSearchable>();
			Action<object[]> resultAction = (m)=>{};
			searcherInterface.Setup(m=> m.GetSourceException()).Returns(typeof(Exception)).Verifiable();
			searcherInterface.Setup(m=> m.GetSourceObject()).Returns(typeof(ICommand)).Verifiable();
			searcherInterface.Setup(m=> m.GetActionInputArray()).Returns(new object[]{}).Verifiable();

			Dictionary<Type, Func<object[], Action<object[]>>> decisionTree = new Dictionary<Type, Func<object[], Action<object[]>>>();
			Dictionary<Type, Action<object[]>> secondLevelTree = new Dictionary<Type, Action<object[]>>();

			Action<object[]> act = (m) => { handled = true; };

			

			//ioc conf
			var defaultStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
			{
				if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else if (key == "ExceptionHandler.GetSecLevelTree")
				{
					return secondLevelTree;
				}
				else if (key == "ExceptionHandler.GetTree")
				{
					return decisionTree;
				}
				else
				{
					throw new Exception("Unknown IoC KEY");
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			//test

			Func<object[], Action<object[]>> foo = delegate (object[] param)
				{
					return IoC.Resolve<Dictionary<Type, Action<object[]>>>("ExceptionHandler.GetSecLevelTree", param[1])[(Type)(param[0])];
				};

			decisionTree.Add(typeof(ICommand), foo);

			
			secondLevelTree.Add(typeof(System.Exception), act);

			HandlerSearchCommand searchCommand = new HandlerSearchCommand(searcherInterface.Object);
			searchCommand.Execute();

			searcherInterface.VerifyAll();
			Assert.True(handled);
			//test ends

			//ioc
			IoC.Resolve<ICommand>("IoC.Setup", defaultStrategy).Execute();
		}
		

		[Fact]
		public void HardStopTest()
		{
			bool threadContinuable = true;
			//IoC setup
			var defaultStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
			{
				if (key == "Thread.CanContinue")
				{
					return threadContinuable;
				}
				else if (key == "Thread.CanContinue.Setup")
				{
					threadContinuable = (bool)args[0];
					return threadContinuable;
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else if (key == "IoC.Strategy")
				{
					return defaultStrategy;
				}
				else
				{
					throw new Exception("Unknown IoC KEY");
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			//test
			var stopable = new Mock<IThreadStopableHard>();
			ICommand stopCmd = new ThreadHardStopCmd(stopable.Object);

			IoC.Resolve<bool>("Thread.CanContinue.Setup", true);
			while (IoC.Resolve<bool>("Thread.CanContinue"))
			{
				stopCmd.Execute();
			}

			stopable.VerifyAll();
			Assert.False(threadContinuable);

			//test's end
			//IoC's last will and testament
			IoC.Resolve<ICommand>("IoC.Setup", defaultStrategy).Execute();
		}

		[Fact]
		public void SoftStopTest()
		{
			bool threadContinuable = true;
			var queue = new BlockingCollection<ICommand>();
			//IoC setup
			var defaultStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
			{
				if (key == "Thread.CanContinue")
				{
					return threadContinuable;
				}
				else if (key == "Thread.CanContinue.Setup")
				{
					threadContinuable = (bool)args[0];
					return threadContinuable;
				}
				else if (key == "Thread.CommandQueue")
				{
					return queue;
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else if (key == "IoC.Strategy")
				{
					return defaultStrategy;
				}
				else
				{
					throw new Exception("Unknown IoC KEY");
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			//test
			var stopable = new Mock<IThreadStopableSoft>();
			ICommand stopCmd = new ThreadSoftStopCmd(stopable.Object);
			queue.Add(stopCmd);

			int numberOfExecutedDefaultCmd = 0;
			var someDefaultCmd = new Mock<ICommand>();
			someDefaultCmd.Setup(m => m.Execute()).Callback(() => numberOfExecutedDefaultCmd++).Verifiable();
			queue.Add(someDefaultCmd.Object);
			queue.Add(someDefaultCmd.Object);
			queue.Add(someDefaultCmd.Object);
			queue.Add(someDefaultCmd.Object);
			queue.Add(someDefaultCmd.Object);

			Action<BlockingCollection<ICommand>> defaultAct = (commandQueue) => {
				ICommand cmd = null;
				try
				{
					cmd = commandQueue.Take();
					cmd.Execute();
				}
				catch (Exception ex)
				{
					IoC.Resolve<ICommand>("ExHandler", cmd.GetType(), ex).Execute();
				}
			};
			Action threadCicle = () =>
			{
				BlockingCollection<ICommand> q = IoC.Resolve<BlockingCollection<ICommand>>("Thread.CommandQueue");
				while (IoC.Resolve<bool>("Thread.CanContinue"))
				{
					IThreadProc.ProcAct(q);
				}
			};

			IThreadProc.ProcAct = defaultAct;
			threadCicle();

			stopable.VerifyAll();
			Assert.False(threadContinuable);
			Assert.Equal(5, numberOfExecutedDefaultCmd);

			//test's end
			//IoC's last will and testament
			IoC.Resolve<ICommand>("IoC.Setup", defaultStrategy).Execute();
		}
	

        //поиск команды
        [Fact]
        public void CollisionDetectCommandTest()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

            var m1 = new Mock<IMatchable>();
            m1.Setup(m1 => m1.Type).Returns("Obj1").Verifiable();
            m1.Setup(m1 => m1.Attributes).Returns(new int[] {2, 3, 4, 4}).Verifiable();

            var m2 = new Mock<IMatchable>();
            m2.Setup(m2 => m2.Type).Returns("Obj2").Verifiable();
            m2.Setup(m2 => m2.Attributes).Returns(new int[] {1, 1, 1, 1}).Verifiable();

            var onSuccess = new Mock<ICommand>();
            var onFail = new Mock<ICommand>();

            Func<string, object[], object> strategy = (key, args) =>
            {
                if ("GetDecisionTreeForObj1AndObj2" == key) {
                    return new Mock<Dictionary<int,object>>().Object;
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };

            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

            var cmd = new CollisionDetectCommand(m1.Object, m2.Object, onSuccess.Object, onFail.Object);
            cmd.Execute();

            m1.VerifyAll();
            m2.VerifyAll();

            IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
        }

        [Fact]
        public void MatchOnSuccess()
        {
            var onSuccess = new Mock<ICommand>();
            onSuccess.Setup(onSuccess => onSuccess.Execute()).Verifiable();
            var onFail = new Mock<ICommand>();

            Dictionary<int, object> tree = new Dictionary<int, object>()
            {
                [1] = new Dictionary<int, object>()
                {
                    [2] = new Dictionary<int, object>()
                    {
                        [3] = new Dictionary<int, object>()
                        {
                            [3] = new Dictionary<int, object>(){}
                        }
                    }
                }
            };

            var matcher = new CollisionMatcher(tree);
            matcher.Match(new int[] {1, 2, 3, 3}, onSuccess.Object, onFail.Object);

            onSuccess.VerifyAll(); 
        }

        [Fact]
        public void MatchOnFail()
        {
            var onSuccess = new Mock<ICommand>();
            var onFail = new Mock<ICommand>();
            onFail.Setup(onFail => onFail.Execute()).Verifiable();

            Dictionary<int, object> tree = new Dictionary<int, object>()
            {
                [1] = new Dictionary<int, object>()
                {
                    [2] = new Dictionary<int, object>()
                    {
                        [3] = new Dictionary<int, object>()
                        {
                            [3] = new Dictionary<int, object>(){}
                        }
                    }
                }
            };

            var matcher = new CollisionMatcher(tree);
            matcher.Match(new int[] {2, 2, 3, 3}, onSuccess.Object, onFail.Object);

            onFail.VerifyAll(); 
        }

        [Fact]
        public void VectorSizeMoreThanTreeSize()
        {
            var onSuccess = new Mock<ICommand>();
            var onFail = new Mock<ICommand>();
            onFail.Setup(onFail => onFail.Execute()).Verifiable();

            Dictionary<int, object> tree = new Dictionary<int, object>()
            {
                [1] = new Dictionary<int, object>()
                {
                    [2] = new Dictionary<int, object>()
                    {
                        [3] = new Dictionary<int, object>()
                        {
                            [3] = new Dictionary<int, object>(){}
                        }
                    }
                }
            };

            var matcher = new CollisionMatcher(tree);
            matcher.Match(new int[] {1, 2, 3, 3, 5}, onSuccess.Object, onFail.Object);

            onFail.VerifyAll(); 
        }

        [Fact]
        public void VectorSizeLessThanTreeSize()
        {
            var onSuccess = new Mock<ICommand>();
            var onFail = new Mock<ICommand>();
            onFail.Setup(onFail => onFail.Execute()).Verifiable();

            Dictionary<int, object> tree = new Dictionary<int, object>()
            {
                [1] = new Dictionary<int, object>()
                {
                    [2] = new Dictionary<int, object>()
                    {
                        [3] = new Dictionary<int, object>()
                        {
                            [3] = new Dictionary<int, object>(){}
                        }
                    }
                }
            };

            var matcher = new CollisionMatcher(tree);
            matcher.Match(new int[] {2, 2, 4}, onSuccess.Object, onFail.Object);

            onFail.VerifyAll(); 
        }

		//Tests from lab5

		[Fact]
		public void ChangeVelocityCommand_Execute_Test()
		{
			Mock<IChangeVelocity> obj = new Mock<IChangeVelocity>();
			obj.Setup(n=>n.GetAngle()).Returns(Math.PI /2).Verifiable();
			obj.Setup(n=> n.GetVelocityModule()).Returns(10).Verifiable();
			obj.Setup(n=> n.SetVelocity(It.Is<MyVector>(n=>n.Equals(new MyVector(0,10))))).Verifiable();

			ChangeVelocityCommand cmd = new ChangeVelocityCommand(obj.Object);
			cmd.Execute();
			obj.VerifyAll();
		}

		[Fact]
		public void TestMoveStartCmd()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

			var _startableMock = new Mock<IMoveStartable>();
            _startableMock.Setup(m => m.GetAngle()).Returns((double)0).Verifiable();
            _startableMock.Setup(m => m.GetSource()).Returns(new Mock<IUObject>().Object).Verifiable();
            _startableMock.Setup(m => m.SetSpeed(It.Is<uint>(s => s == 5))).Verifiable();
            _startableMock.Setup(m => m.SetVelocity(It.Is<MyVector>(s => s.Equals(new MyVector(5,0))))).Verifiable();
            _startableMock.Setup(m => m.SetMoveInjector(It.IsAny<ICommand>())).Verifiable();

            var start = new MoveStartCommand(_startableMock.Object, 5);

            Func<string, object[], object> strategy = (key, args) =>
            {
                if (key == "MoveCommand")
                {
                    var command = new Mock<ICommand>();
                    return command.Object;
                }
                else if (key == "push")
                {
                    return new InjectorCommand((ICommand)args[0]);
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };

            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();
            
            start.Execute();
            
            _startableMock.VerifyAll();
			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
        public void IoCResolveShootCmdTest()
        {
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

			var m = new Mock<IShootable>();
            ShootCommand shootCmd = new ShootCommand(m.Object);

            m.Setup(m => m.InitialBulletDirection).Returns(5).Verifiable();
            m.Setup(m => m.InitialBulletPosition).Returns(new MyVector(5, 10)).Verifiable();
            m.Setup(m => m.InitialVelocity).Returns(new MyVector(5, 10)).Verifiable();

            bool isCreateBulletOk = true;
            bool createBulletWasCalled = true;
            var createBullet = new Mock<IUObject>();

            bool isSetPositionOk = true;
            bool setPositionWasCalled = true;
            var setPosition = new Mock<ICommand>();

            bool isSetDirectionOk = true;
            bool setDirectionWasCalled = true;
            var setDirecton = new Mock<ICommand>();

            bool isActionOk = true;
            bool actionWasCalled = true;
            var action = new Mock<IUObject>();

            bool isStartMoveOk = true;
            bool startMoveWasCalled = true;
            var startMove = new Mock<ICommand>();
            
            Func<string, object[], object> strategy = (key, args) =>
            {
                if ("CreateBullet" == key)
                {
                    if (!(args.Length == 0))
                    {
                        isCreateBulletOk = false;
                    }
                    return createBullet.Object;
                }

                else if ("SetPosition" == key)
                {
                    if (args.Length == 2)
                    {
                        if (!(args[0] is IUObject && args[1] is MyVector))
                        {
                            isSetPositionOk = false;
                        }
                    }
                    return setPosition.Object;
                }

                else if ("SetDirection" == key)
                {
                    if (args.Length == 2)
                    {
                        if (!(args[0] is IUObject && args[1] is int))
                        {
                            isSetDirectionOk = false;
                        }
                    }
                    return setDirecton.Object;
                }

                else if ("Action" == key)
                {
                    if (args.Length == 3)
                    {
                        if (!(args[0] is IUObject && (string)args[1] == "Move" && args[2] is MyVector))
                        {
                            isActionOk = false;
                        }
                    }
                    return action.Object;
                }

                else if ("StartMove" == key)
                {
                    if (args.Length == 1)
                    {
                        if (!(args[0] is IUObject))
                        {
                            isStartMoveOk = false;
                        }
                    }
                    return startMove.Object;
                }

                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };

            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

            shootCmd.Execute();

            m.VerifyAll();

            Assert.True(createBulletWasCalled && isCreateBulletOk);
            Assert.True(setPositionWasCalled && isSetPositionOk);
            Assert.True(setDirectionWasCalled && isSetDirectionOk);
            Assert.True(actionWasCalled && isActionOk);
            Assert.True(startMoveWasCalled && isStartMoveOk);

			IoC.Resolve<ICommand>("IoC.Setup",preStrategy).Execute();
		}

		[Fact]
		public void MacroCommand_GetSize_Test()
		{
			Mock<ICommand> mockCommand = new Mock<ICommand>();
			mockCommand.Setup(a => a.Execute()).Verifiable();

			MacroCommand macro = new MacroCommand(new List<ICommand>{mockCommand.Object, mockCommand.Object});
			Assert.Equal(2, macro.GetSize());
		}

		[Fact]
		public void MacroCommand_DefaultConstructor_Test() {
			MacroCommand macro = new MacroCommand();

			Assert.Equal(0, macro.GetSize());
		}

		[Fact]
		public void MacroCommand_ExecuteTest()
		{
			Mock<ICommand> mockCommand = new Mock<ICommand>();
			mockCommand.Setup(a=>a.Execute()).Verifiable();

			MacroCommand macro = new MacroCommand(new List<ICommand>{mockCommand.Object, mockCommand.Object});
			macro.Execute();

			mockCommand.VerifyAll();
			Assert.Equal(2, macro.GetSize());
		}

		[Fact]
		public void TestMoveStart180Cmd()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
            {
                if (key == "MoveCommand")
                {
                    var command = new Mock<ICommand>();
                    return command.Object;
                }
                else if (key == "push")
                {
                    return new InjectorCommand((ICommand)args[0]);
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();
			var _startableMock = new Mock<IMoveStartable>();
            _startableMock.Setup(m => m.GetAngle()).Returns(Math.PI).Verifiable();
            _startableMock.Setup(m => m.GetSource()).Returns(new Mock<IUObject>().Object).Verifiable();
            _startableMock.Setup(m => m.SetSpeed(It.Is<uint>(s => s == 5))).Verifiable();
            _startableMock.Setup(m => m.SetVelocity(It.Is<MyVector>(s => s.Equals(new MyVector(-5,0))))).Verifiable();
            _startableMock.Setup(m => m.SetMoveInjector(It.IsAny<ICommand>())).Verifiable();

            var start = new MoveStartCommand(_startableMock.Object, 5);
            
            start.Execute();
            
            _startableMock.VerifyAll();
			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
		public void TestMoveStart90Cmd()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
            {
                if (key == "MoveCommand")
                {
                    var command = new Mock<ICommand>();
                    return command.Object;
                }
                else if (key == "push")
                {
                    return new InjectorCommand((ICommand)args[0]);
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();
			var _startableMock = new Mock<IMoveStartable>();
            _startableMock.Setup(m => m.GetAngle()).Returns(Math.PI/2).Verifiable();
            _startableMock.Setup(m => m.GetSource()).Returns(new Mock<IUObject>().Object).Verifiable();
            _startableMock.Setup(m => m.SetSpeed(It.Is<uint>(s => s == 5))).Verifiable();
            _startableMock.Setup(m => m.SetVelocity(It.Is<MyVector>(s => s.Equals(new MyVector(0,5))))).Verifiable();
            _startableMock.Setup(m => m.SetMoveInjector(It.IsAny<ICommand>())).Verifiable();

            var start = new MoveStartCommand(_startableMock.Object, 5);
            
            start.Execute();
            
            _startableMock.VerifyAll();
			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
		public void TestMoveStart45Cmd()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");
			Func<string, object[], object> strategy = (key, args) =>
            {
                if (key == "MoveCommand")
                {
                    var command = new Mock<ICommand>();
                    return command.Object;
                }
                else if (key == "push")
                {
                    return new InjectorCommand((ICommand)args[0]);
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			var _startableMock = new Mock<IMoveStartable>();
            _startableMock.Setup(m => m.GetAngle()).Returns(Math.PI/4).Verifiable();
            _startableMock.Setup(m => m.GetSource()).Returns(new Mock<IUObject>().Object).Verifiable();
            _startableMock.Setup(m => m.SetSpeed(It.Is<uint>(s => s == 5))).Verifiable();
            _startableMock.Setup(m => m.SetVelocity(It.Is<MyVector>(s => s.Equals(new MyVector(3,3))))).Verifiable();
            _startableMock.Setup(m => m.SetMoveInjector(It.IsAny<ICommand>())).Verifiable();

            var start = new MoveStartCommand(_startableMock.Object, 5);
            start.Execute();
            
            _startableMock.VerifyAll();
			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
		public void TestMoveStopCmd()
		{
			var _stopableMock = new Mock<IMoveStopable>();
            _stopableMock.Setup(m => m.GetMoveInjector()).Returns(new InjectorCommand(new Mock<ICommand>().Object)).Verifiable();
            _stopableMock.Setup(m => m.ClearVelocity()).Verifiable();
            _stopableMock.Setup(m => m.ClearSpeed()).Verifiable();

            var start = new MoveStopCommand(_stopableMock.Object);
            
            start.Execute();
            
            _stopableMock.VerifyAll();
		}

		[Fact]
		public void TestMoveCmd()
		{
			var _movableMock = new Mock<IMovable>();
            _movableMock.Setup(m => m.GetVelocity()).Returns(new MyVector(4,3)).Verifiable();
            _movableMock.Setup(m => m.GetPosition()).Returns(new MyVector(1,1)).Verifiable();
            _movableMock.Setup(m => m.SetPosition(It.Is<MyVector>(p => p.Equals(new MyVector(5,4))))).Verifiable();

            var move = new MoveCommand(_movableMock.Object);
            
            move.Execute();
            
            _movableMock.VerifyAll();
		}

		[Fact]
		public void TestMoveCmd2()
		{
			var _movableMock = new Mock<IMovable>();
            _movableMock.Setup(m => m.GetVelocity()).Returns(new MyVector(-1,3)).Verifiable();
            _movableMock.Setup(m => m.GetPosition()).Returns(new MyVector(1,-1)).Verifiable();
            _movableMock.Setup(m => m.SetPosition(It.Is<MyVector>(p => p.Equals(new MyVector(0,2))))).Verifiable();

            var move = new MoveCommand(_movableMock.Object);
            
            move.Execute();
            
            _movableMock.VerifyAll();
		}

		 [Fact]
        public void IRotable_Set_Test()
        {
			Mock<IRotable> objRotable = new Mock<IRotable>();
			objRotable.Setup(n => n.GetAngle()).Returns((-1)*Math.PI/2).Verifiable();
			objRotable.Setup(n => n.GetRortationVelocity()).Returns(Math.PI).Verifiable();
			objRotable.Setup(n => n.SetAngle(It.Is<double>(a=>Math.Round(a,3)==Math.Round(Math.PI/2,3)))).Verifiable();

			RotateCommand rotateCmd = new RotateCommand(objRotable.Object);
			rotateCmd.Execute();
			objRotable.VerifyAll();
		}

		[Fact]
		public void StartRotateCommand(){
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

			Func<string, object[], object> strategy = (key, args) =>
            {
                if (key == "MoveCommand")
                {
                    var command = new Mock<ICommand>();
                    return command.Object;
                }
                else if (key == "push")
                {
                    return new InjectorCommand((ICommand)args[0]);
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>) args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }else if(key == "StartRotationMacro")
				{
					return new MacroCommand();
				}
                else
                {
                    throw new Exception();
                }
            };
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			Mock<IStartableRotation> startableObj = new Mock<IStartableRotation>();
			startableObj.Setup(a=>a.SetRortationVelocity(It.Is<double>(a=>a==10))).Verifiable();
			startableObj.Setup(n=>n.SetRotationInjector(It.IsAny<InjectorCommand>())).Verifiable();

			

			StartRotateCommand startRotation = new StartRotateCommand(startableObj.Object, 10);
			startRotation.Execute();

			startableObj.VerifyAll();
			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
		public void StopRotate_Execute_Test()
		{
			Mock<IStopableRotation> rotationStopableObject = new Mock<IStopableRotation>();
			rotationStopableObject.Setup(a => a.SetRortationVelocity(It.Is<double>(a => a == 0))).Verifiable();
			Mock<ICommand> mockCommand = new Mock<ICommand>();
			mockCommand.Setup(a => a.Execute()).Verifiable();

			MacroCommand macro = new MacroCommand(new List<ICommand> { mockCommand.Object, mockCommand.Object });

			InjectorCommand injector = new InjectorCommand(macro);

			StopRotationCommand stopCommand = new StopRotationCommand(/*ref*/ rotationStopableObject.Object,ref injector);
			stopCommand.Execute();

			rotationStopableObject.VerifyAll();
		}

		[Fact]
		public void OrdersInretpritationTacticsTest()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

			var someCommand = new Mock<ICommand>();
			someCommand.Setup(m => m.Execute());
			
			var tactics = new Mock<IIoCTactics>();
			tactics.Setup(m => m.Exequte<ICommand>(It.IsAny<IUObject>())).Returns(someCommand.Object).Verifiable();
			
			string orderType = "orderType";

			Dictionary<string, IIoCTactics> interpritationTacticsCollection = new Dictionary<string, IIoCTactics>();
			interpritationTacticsCollection.Add(orderType, tactics.Object);

			var order = new Mock<IUObject>();

			Func<string, object[], object> strategy = (key, args) =>
			{
				if(key == "Orders.InterpritationTactics.GetCollection")
				{
					return interpritationTacticsCollection;
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else
				{
					throw new Exception("Unknown IoC KEY! Key was " + key);
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			OrdersInterpretationTactics interpretationTactics = new OrdersInterpretationTactics();
			interpretationTactics.Exequte<ICommand>(orderType, order.Object).Execute();

			tactics.VerifyAll();
			order.VerifyAll();

			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
		public void OrderMoveStartAdapter_Test()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

			var startableMock = new Mock<IMoveStartable>();
            startableMock.Setup(m => m.GetAngle()).Returns(Math.PI/4).Verifiable();
            startableMock.Setup(m => m.GetSource()).Returns(new Mock<IUObject>().Object).Verifiable();
            startableMock.Setup(m => m.SetSpeed(It.Is<uint>(s => s == 5))).Verifiable();
            startableMock.Setup(m => m.SetVelocity(It.Is<MyVector>(s => s.Equals(new MyVector(3,3))))).Verifiable();
            startableMock.Setup(m => m.SetMoveInjector(It.IsAny<ICommand>())).Verifiable();

			var order = new Mock<IUObject>();
			order.Setup(m=>m["IMoveStartable"]).Returns(startableMock.Object).Verifiable();

			Func<string, object[], object> strategy = (key, args) =>
			{
				if(key == "GetMoveStartCommand")
				{
					return new MoveStartCommand((IMoveStartable)args[0], 5);
				}
				else if(key == "MoveCommand")
				{
					return new EmptyCommand();
				}
				else if(key == "push")
				{
					return new EmptyCommand();
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else
				{
					throw new Exception("Unknown IoC KEY! Key was " + key);
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			OrderMoveStartAdapter tactics = new OrderMoveStartAdapter();
			tactics.Exequte<ICommand>(order.Object).Execute(); 

			startableMock.Verify();
			order.VerifyAll();

			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
		public void OrderMoveStopAdapter_Test()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

			var stopableMock = new Mock<IMoveStopable>();
            stopableMock.Setup(m => m.GetMoveInjector()).Returns(new InjectorCommand(new Mock<ICommand>().Object)).Verifiable();
            stopableMock.Setup(m => m.ClearVelocity()).Verifiable();
            stopableMock.Setup(m => m.ClearSpeed()).Verifiable();

			var order = new Mock<IUObject>();
			order.Setup(m=>m["IMoveStopable"]).Returns(stopableMock.Object).Verifiable();

			Func<string, object[], object> strategy = (key, args) =>
			{
				if(key == "GetMoveStopCommand")
				{
					return new MoveStopCommand((IMoveStopable)args[0]);
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else
				{
					throw new Exception("Unknown IoC KEY! Key was " + key);
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			OrderMoveStopAdapter tactics = new OrderMoveStopAdapter();
			tactics.Exequte<ICommand>(order.Object).Execute(); 

			stopableMock.Verify();
			order.VerifyAll();

			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
		public void OrderStartRotationtAdapter_Test()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

			Mock<IStartableRotation> startableObj = new Mock<IStartableRotation>();
			startableObj.Setup(a=>a.SetRortationVelocity(It.Is<double>(a=>a==10))).Verifiable();
			startableObj.Setup(n=>n.SetRotationInjector(It.IsAny<InjectorCommand>())).Verifiable();

			var order = new Mock<IUObject>();
			order.Setup(m=>m["IStartableRotation"]).Returns(startableObj.Object).Verifiable();

			Func<string, object[], object> strategy = (key, args) =>
			{
				if(key == "GetStartRotationCommand")
				{
					return new StartRotateCommand((IStartableRotation)args[0], 10);
				}
				else if(key == "StartRotationMacro")
				{
					return new EmptyCommand();
				}
				else if(key == "push")
				{
					return new EmptyCommand();
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else
				{
					throw new Exception("Unknown IoC KEY! Key was " + key);
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			OrderStartRotationAdapter tactics = new OrderStartRotationAdapter();
			tactics.Exequte<ICommand>(order.Object).Execute(); 

			startableObj.Verify();
			order.VerifyAll();

			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
		public void OrderStopRotationAdapter_Test()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

			Mock<IStopableRotation> rotationStopableObject = new Mock<IStopableRotation>();
			rotationStopableObject.Setup(a => a.SetRortationVelocity(It.Is<double>(a => a == 0))).Verifiable();
			Mock<ICommand> mockCommand = new Mock<ICommand>();
			mockCommand.Setup(a => a.Execute()).Verifiable();

			MacroCommand macro = new MacroCommand(new List<ICommand> { mockCommand.Object, mockCommand.Object });

			InjectorCommand injector = new InjectorCommand(macro);

			var order = new Mock<IUObject>();
			order.Setup(m=>m["IStopableRotation"]).Returns(rotationStopableObject.Object).Verifiable();

			Func<string, object[], object> strategy = (key, args) =>
			{
				if(key == "GetStopRotationCommand")
				{
					return new StopRotationCommand((IStopableRotation)args[0], ref injector);
				}
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else
				{
					throw new Exception("Unknown IoC KEY! Key was " + key);
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			OrderStopRotationAdapter tactics = new OrderStopRotationAdapter();
			tactics.Exequte<ICommand>(order.Object).Execute(); 

			rotationStopableObject.Verify();
			order.VerifyAll();

			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

		[Fact]
		public void OrderShootAdapter_Test()
		{
			var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

			var shootable = new Mock<IShootable>();

            shootable.Setup(m => m.InitialBulletDirection).Returns(5).Verifiable();
            shootable.Setup(m => m.InitialBulletPosition).Returns(new MyVector(5, 10)).Verifiable();
            shootable.Setup(m => m.InitialVelocity).Returns(new MyVector(5, 10)).Verifiable();

			var order = new Mock<IUObject>();
			order.Setup(m=>m["IShootable"]).Returns(shootable.Object).Verifiable();

            var createBullet = new Mock<IUObject>();
            var setPosition = new Mock<ICommand>();
            var setDirecton = new Mock<ICommand>();
            var action = new Mock<IUObject>();
            var startMove = new Mock<ICommand>();

			Func<string, object[], object> strategy = (key, args) =>
			{
				if(key == "GetShootCommand")
				{
					return new ShootCommand((IShootable)args[0]);
				}
				else if ("CreateBullet" == key)
                {
                    return createBullet.Object;
                }
                else if ("SetPosition" == key)
                {
                    return setPosition.Object;
                }
                else if ("SetDirection" == key)
                {
                    return setDirecton.Object;
                }
                else if ("Action" == key)
                {
                    return action.Object;
                }
                else if ("StartMove" == key)
                {
                    return startMove.Object;
                }
				else if (key == "IoC.Setup")
				{
					var newStrategy = (Func<string, object[], object>)args[0];
					return new IoC.IoCSetupCommand(newStrategy);
				}
				else
				{
					throw new Exception("Unknown IoC KEY! Key was " + key);
				}

			};
			IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

			OrderShootAdapter tactics = new OrderShootAdapter();
			tactics.Exequte<ICommand>(order.Object).Execute(); 

			shootable.Verify();
			order.VerifyAll();

			IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
		}

    

        [Fact]
        public void IoCResolveTest()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

            bool cmd2WasRequested = false;
            bool logWasRequested = false;

            var moveCmd = new Mock<ICommand>();
            moveCmd.Setup(cmd => cmd.Execute()).Callback(() =>
            {
                throw new ArgumentException();  
            }   
            );

            var inter = new Mock<IMoveExceptionHandler>();
            inter.Setup(inter => inter.cmd).Returns(moveCmd.Object);
            inter.Setup(inter => inter.type).Returns(typeof(ArgumentException)); 

            var cmd2 = new MoveCommandExHandlerSecond(inter.Object);

            Func<string, object[], object> strategy = (key, args) =>
            {
                if ("GiveCommand2" == key)
                {
                    cmd2WasRequested = true;
                    return cmd2;
                }
                else if ("Log.Message" == key)
                {
                    logWasRequested = true;
                    return new Logger(args[0].ToString());
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>)args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };

            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

            var cmd1 = new MoveCommandExHandlerFirst(inter.Object);

            cmd1.Execute();
        
            Assert.True(cmd2WasRequested);
            Assert.True(logWasRequested);

            IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
        }

        [Fact]
        public void ExceptionAppearseOneTime()
        {
            
            var moveCmd = new Mock<ICommand>();
            var inter = new Mock<IMoveExceptionHandler>();
            inter.Setup(inter => inter.cmd).Returns(moveCmd.Object).Verifiable();
            inter.Setup(inter => inter.type).Returns(typeof(ArgumentException)).Verifiable();

            int counter = 0;
            moveCmd.Setup(moveCmd => moveCmd.Execute()).Callback(() =>
            {
                counter++;
                if (counter == 1)
                {
                    throw new Exception();
                }
            }   
            );

            var cmd1 = new MoveCommandExHandlerFirst(inter.Object);

            Assert.Throws<Exception>(()=>cmd1.Execute());
            Assert.Equal(1, counter);
            
            inter.VerifyAll();
        }

        [Fact]
        public void ExceptionAppearseTwoTimes()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

            var moveCmd = new Mock<ICommand>();
            int counter = 0;
            moveCmd.Setup(moveCmd => moveCmd.Execute()).Callback(() =>
            {
                counter++;
                if (counter == 1)
                {
                    throw new ArgumentException();
                }
                else if (counter == 2)
                {
                    throw new Exception();
                }
            }   
            );
            
            var inter = new Mock<IMoveExceptionHandler>();
            inter.Setup(inter => inter.cmd).Returns(moveCmd.Object).Verifiable();
            inter.Setup(inter => inter.type).Returns(typeof(ArgumentException)).Verifiable();

            Func<string, object[], object> strategy = (key, args) =>
            {
                if ("GiveCommand2" == key)
                {
                    return new MoveCommandExHandlerSecond((IMoveExceptionHandler)args[0]);
                }
                else if ("Log.Message" == key)
                {
                    return new Logger(args[0].ToString());
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>)args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };

            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

            var cmd1 = new MoveCommandExHandlerFirst(inter.Object);

            Assert.Throws<Exception>(()=>cmd1.Execute());
            Assert.Equal(2, counter);
            
            inter.VerifyAll();

            IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
        }

        [Fact]
        public void ExceptionAppearseTwoTimesAndLog()
        {
            var preStrategy = IoC.Resolve<Func<string, object[], object>>("IoC.Strategy");

            bool logWasRequested = false;

            var moveCmd = new Mock<ICommand>();
            int counter = 0;
            moveCmd.Setup(moveCmd => moveCmd.Execute()).Callback(() =>
            {
                counter++;
                if (counter == 1)
                {
                    throw new ArgumentException();
                }
                else if (counter == 2)
                {
                    throw new ArgumentException();
                }
            }   
            );

            var inter = new Mock<IMoveExceptionHandler>();
            inter.Setup(inter => inter.cmd).Returns(moveCmd.Object).Verifiable();
            inter.Setup(inter => inter.type).Returns(typeof(ArgumentException)).Verifiable();

            Func<string, object[], object> strategy = (key, args) =>
            {
                if ("GiveCommand2" == key)
                {
                    return new MoveCommandExHandlerSecond((IMoveExceptionHandler)args[0]);
                }
                else if ("Log.Message" == key)
                {
                    logWasRequested = true;
                    return new Logger(args[0].ToString());
                }
                else if ("IoC.Setup" == key)
                {
                    var newStrategy = (Func<string, object[], object>)args[0];
                    return new IoC.IoCSetupCommand(newStrategy);
                }
                else
                {
                    throw new Exception();
                }
            };

            IoC.Resolve<ICommand>("IoC.Setup", strategy).Execute();

            var cmd1 = new MoveCommandExHandlerFirst(inter.Object);

            cmd1.Execute();

            Assert.True(logWasRequested);
            Assert.Equal(2, counter);
            
            inter.VerifyAll();

            IoC.Resolve<ICommand>("IoC.Setup", preStrategy).Execute();
        }
    }
}
